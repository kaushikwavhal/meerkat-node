var MongoClient = require("mongodb").MongoClient;
var ObjectID = require("mongodb").ObjectID;

var http = require('http');
var request = require("request");

function startProcess() {

	request( {uri:"https://resources.meerkatapp.co/broadcasts",followAllRedirects:true}, function(error, response, body) {

        if(error){
            console.log("Error: ",error);
            getTweetsFromDB(null);
            return;
            //return "Wrong URL";
        }
		var arr=[];
		var result=JSON.parse(body);
		result.result.forEach(function (element, index, array) {
			arr.push(element.id);
		});
        getTweetsFromDB(arr);  
    });

}

startProcess(); //run process once
setInterval(startProcess,60000); //continue to loop after 60sec interval i.e 60000 ms

function getTweetsFromDB(arr)
{
	MongoClient.connect("mongodb://localhost/meerkat_tweet", function(err, db) {

		// if we didn't connect, throw the error
		if (err) throw err


		// in mongo, documents are grouped in collection (like a table)
		// lets make one!
		var count=0;

		var collection = db.collection("tweet");
	  
		var id;


	   collection.find().toArray(function(e, d) {
				console.log("=============================================================");
				console.log(new Date());
			console.log("Total Tweets in the DB = "+d.length);
			db.close();
			deleteTweets(arr,d);
		});
	  
	});
}

function deleteTweets(live,curr)
{
	console.log("liveStreamsCount=",live.length);
	console.log("CurrentDBTweetsCount=",curr.length);

	var inactiveStreams=[];
	//Compare the live and current tweets in the DB, and filter out the inactive once's
	curr.forEach(function(element, index, array){

		if(live.indexOf(element.meerkat_id)<0)
		{
		
			inactiveStreams.push(element._id);
			
		}
		else if(typeof element.meerkat_id=='undefined' || element.meerkat_id==null || element.meerkat_id=='')
		{
			inactiveStreams.push(element._id);
		}
	});
				
	console.log("InactiveStreamsCount=",inactiveStreams.length);	
	
	MongoClient.connect("mongodb://localhost/meerkat_tweet", function(err, db) {
	    // if we didn't connect, throw the error
		if (err)
		{
			console.log( err);
			return;
		}
		
		
		var collection2 = db.collection("tweet");
		
		collection2.remove({'_id': {'$in':inactiveStreams}}, function(err2, result2) {
			if (err2) {
				console.log(err2);
			}
			//console.log(result);
			console.log("cleared inactive streams!!");
			db.close();
		});
	});			

}

